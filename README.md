# Interfaces in Go

Interfaces are like an abstract type that allows you to attach methods to different types, assuming that the given type matches the method signatures of the interface. An example will be more clear.

First, we will define our interface, `shape`:
```golang
type shape interface {
    area() float64
}
```
Any type that implements an `area()` function that returns a `float64` will be a `shape`.

Now lets create two shapes, `circle`, and `rect`:
```golang
type circle struct {
    raidus float64
}

func (c circle) area() float64 {
    return math.Pi * c.radius * c.radius
}

type rect struct {
    width float64
    height float64
}

func (r rect) area() float64 {
    return r.width * r.width
}
```

Since both `circle` and `rect` implement `area() float64`, we can refer to them as `shape`:
```golang
c1 := circle{4, 5}
r1 := rect{5, 7}
shapes := []shape{c1, r1}

for _, shape := range shapes {
    fmt.Println(shape.area())
}
```

Now we can create a list of `shape` with different shape types in it, since `shape` is a generic bucket for anything that matches. Neato-burrito